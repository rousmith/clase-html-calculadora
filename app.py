from flask import *

app= Flask (__name__)

@app.route ('/')
def index():
    return render_template('index.html')

@app.route ('/sumar', methods=['POST'])
def sumar ():
    misDatos=request.get_json ()

    a=misDatos ["numero1"]
    b=misDatos ["numero2"]
    resultado= a+b 
    return jsonify ({
        "resultado": resultado
    })


@app.route('/saludar')
def saludar ():
    return jsonify ({
        "saludo": "hola clase"
    })
app.run ()